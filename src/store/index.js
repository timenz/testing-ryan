import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        names: []
    },
    getters: {
        names(state) {
            return state.names
        }
    },
    mutations: {
        addNewName(state, newName) {
            state.names.push(newName)
        }
    }
})