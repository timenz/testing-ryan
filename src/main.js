import Vue from 'vue'
// import App from './App.vue'
// import App from './MyApp.vue'
import App from './MyAppVuex.vue'
import store from './store/index'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
